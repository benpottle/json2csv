package main

import (
	"bufio"
	"log"
	"os"
	"strings"
)

func main() {
	printHeader := false
	if len(os.Args) > 2 && os.Args[2] == "--header" {
		printHeader = true
	}

	json, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer json.Close()

	outputFile, err := os.OpenFile(os.Args[1]+".csv", os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer outputFile.Close()
	fileWriter := bufio.NewWriter(outputFile)

	scanner := bufio.NewScanner(json)
	for scanner.Scan() {
		header := ""
		values := ""

		nrParts := strings.Split(strings.TrimSpace(scanner.Text()), ",")

		for _, value := range nrParts {
			dataParts := strings.Split(value, ":")

			if len(dataParts) >= 2 {
				header += dataParts[0] + ","
				values += dataParts[1] + ","
			}
		}

		// remove trailing commas and add newlines
		header = header[:len(header)-1] + "\n"
		// also remove trailing } for values line
		values = values[:len(values)-2] + "\n"

		if printHeader {
			fileWriter.WriteString(header)
			printHeader = false
		}

		// write the output
		fileWriter.WriteString(values)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
