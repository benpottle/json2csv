A simple CLI tool to convert JSON data to CSV with an emphasis on speed.

USAGE:

In command prompt run the following command:

json2csv.exe <path to json file> [--header]

--header is an optional flag to include a header row at the top of the output csv file.

json2csv will produce an output file beside the input file named <path to json file>.csv (same name with a .csv extenstion)